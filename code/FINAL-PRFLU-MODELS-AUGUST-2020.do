program define PRflu-finalmodels

/

*
commands used to produce results in R&R version of paper
produced on July/August 2020

1. We use the data file FINAL-PRFLU-DATA-AUGUST.dta which contains a few extra variables we will us
to do boxplots

2. We estimates main models as follows:

	2.1. Using flu ONLY (no severity)
		
		2.1.1 Female as additive effect
		2.1.2.Female in interaction
		
	2.2. Using flu AND newseverity: female in interactions

3. Females only: As in (2.2) BUT only for females

4. Models with flu and earthquake severity

5. We estimate permutations as follows:
	
	We use permute and treat the regression coefficient of the covariate of interest as the 
	quantity whose distribution we are trying to get at.
	
*/

use FINAL-PRFLU-DATA-AUGUST.dtaxtset code1910

log using PRflu-results-7-2020

/************************************************************************/
/*************************MODELS FOR TABLES IN FINAL PAPER***************/
/************************************************************************/


/*Model for Table 3: knee and height using only flu and interaction female effects*/

/*KNEE*/
xtreg knee yeduca emale i.dummyF_1,vce(robust) fe
predict pknee_flu_NOsever_all

xtreg knee yeduca i.dummyF_1##i.female,vce(robust) fe
predict pknee_flu_NOsever_all

test _b[1.dummyF_1]+ _b[1.dummyF_1#.female]=0
local beta1knee=_b[1.dummyF_1]+ _b[1.dummyF_1#1.female]

/*

NOT DONE

permute knee perm1knee=_b[1.dummyF_1], saving(perm1knee.dta, replace) reps(1000) left nodrop:xtreg knee yeduca i.female##i.dummyF_1, vce(robust) fe

permute knee perm2knee=_b[1.dummyF_1#1.female], saving(perm2knee.dta, replace) reps(1000) left nodrop:xtreg knee yeduca i.female##i.dummyF_1, vce(robust) fe

*/


/*HEIGHT*/

xtreg adjheight yeduca female i.dummyF_1,vce(robust) fe

xtreg adjheight yeduca i.dummyF_1##i.female,vce(robust) fe
predict pflu_flu_NOsever_all

local beta1height=_b[1.dummyF_1]+ _b[1.dummyF_1#1.female]
test _b[1.dummyF_1]+ _b[1.dummyF_1#.female]=0

/*

NOT DONE

permute adjheight perm1height=_b[1.dummyF_1], saving(perm1height.dta, replace) reps(1000) left nodrop:xtreg height yeduca i.female##i.dummyF_1, vce(robust) fe
permute adjheight perm2height=_b[1.dummyF_1#1.female], saving(perm2height.dta, replace) reps(1000) left nodrop:xtreg height yeduca i.female##i.dummyF_1, vce(robust) fe

*/

/*Model for Table 4: knee and height using only flu AND NEWSEVERITY and interaction female effects*/

/*KNEE*/

xtreg knee yeduca  i.dummyF_1##i.newseverity##i.female,vce(robust) fe
predict pknee_flu_sever_all

local beta3knee=_b[1.dummyF_1] + _b[1.dummyF_1#1.female] + _b[1.dummyF_1#1.newseverity] +_b[1.dummyF_1#1.female#1.newseverity]=0

test _b[1.dummyF_1] + _b[1.dummyF_1#1.female] + _b[1,dummyF_1#1.newseverity] +_b[1.dummyF_1#1.female#1.newseverity]=0


/*

NOT DONE
permute knee perm3knee=_b[1.dummyF_1], saving(perm3knee.dta, replace) reps(1000) left nodrop:xtreg knee yeduca  i.female##i.dummyF_1##i.newseverity, vce(robust) fe
permute knee perm4knee=_b[1.dummyF_1#1.female], saving(perm4knee.dta, replace) reps(1000) left nodrop:xtreg knee yeduca i.female##i.dummyF_1##i.newseverity, vce(robust) fe
permute knee perm5knee=_b[1.dummyF_1#1.female#1.newseverity], saving(perm5knee.dta, replace) reps(1000) left nodrop:xtreg knee yeduca i.female##i.dummyF_1##i.newseverity, vce(robust) fe
permute knee perm6knee=_b[1.dummyF_1#1.female#1.newseverity], saving(perm6knee.dta, replace) reps(1000) left nodrop:xtreg knee yeduca i.female##i.dummyF_1##i.newseverity, vce(robust) fe

*/

/*HEIGHT*/

xtreg adjheight yeduca i.dummyF_1##i.newseverity##i.female,vce(robust) fe
predict pheight_flu_sever_all

local beta2height=_b[1.dummyF_1] + _b[1.dummyF_1#1.female] + _b[1,dummyF_1#1.newseverity] +_b[1.dummyF_1#1.female#1.newseverity]=0

test _b[1.dummyF_1] + _b[1.dummyF_1#1.female] + _b[1,dummyF_1#1.newseverity] +_b[1.dummyF_1#1.female#1.newseverity]=0

/*

NOT DONE
permute adjheight perm3height=_b[1.dummyF_1], saving(perm3height.dta, replace) reps(1000) left nodrop:xtreg adjheight yeduca  i.female##i.dummyF_1##i.newseverity, vce(robust) fe
permute adjheight perm4height=_b[1.dummyF_1#1.female], saving(perm4height.dta, replace) reps(1000) left nodrop:xtreg adjheight yeduca i.female##i.dummyF_1##i.newseverity, vce(robust) fe
permute adjheight perm5height=_b[1.dummyF_1#1.female#1.newseverity], saving(perm5height.dta, replace) reps(1000) left nodrop:xtreg adjheight yeduca i.female##i.dummyF_1##i.newseverity, vce(robust) fe
permute adjheight perm6height=_b[1.dummyF_1#1.female#1.newseverity], saving(perm6height.dta, replace) reps(1000) left nodrop:xtreg adjheight yeduca i.female##i.dummyF_1##i.newseverity, vce(robust) fe

*/



/*Model for Table 5: females ONLY and use of dummy for SEVERITY*/


/*KNEE: permuations done but not reported in paper*/

xtreg knee yeduca i.dummyF_1##i.severity if female==1,vce(robust) fe
predict pknee_flu_sever_female

test _b[1.dummyF_1] + _b[1.dummyF_1#3.severity]=0

local gamma=_b[1.dummyF_1] + _b[1.dummyF_1#3.severity]

permute knee perm7knee=_b[1.dummyF_1#3.severity], saving(perm7knee.dta, replace) reps(1000) left nodrop:xtreg knee yeduca i.dummyF_1##i.severity if female==1, vce(robust) fe

/*HEIGHT permutations done but not reported in paper*/

xtreg adjheight yeduca i.dummyF_1##i.severity if female==1,vce(robust) fe
predict pheight_flu_sever_female
test _b[1.dummyF_1] + _b[1.dummyF_1#3.severity]=0

local gamma3height=_b[1.dummyF_1] + _b[1.dummyF_1#3.severity]

permute adjheight perm7height=_b[1.dummyF_1#3.severity], saving(perm7heightheight.dta, replace) reps(1000) left nodrop:xtreg adjheight yeduca i.female##i.dummyF_1##i.severity, vce(robust) fe


/*sixth table: knee and height using flu and earthquake and TESTseverity and female interaction effects*/ 

/*KNEE*/

xtreg knee yeduca i.dummyF_1##i.female##i.testseverity##dummy_earthquake_1,vce(robust) fe

predict pknee_fluearth_testseverity_all

test  _b[1.dummyF_1] +_b[ 1.dummyF_1#1.female] + _b[1.dummyF_1#1.testseverity] + _b[1.dummyF_1#1.female#1.testseverity] + _b[1.dummyF_1#1.dummy_ earthquake_1]+_b[1.dummyF_1#1.female#1.dummy_earthquake_1]+ _b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1] +_b[1.dummyF_1#1.female#1.testseverity#1.dummy_earthquake_1]=0


local beta3knee= _b[1.dummyF_1] +_b[ 1.dummyF_1#1.female] + _b[1.dummyF_1#1.testseverity] + _b[1.dummyF_1#1.female#1.testseverity] + _b[1.dummyF_1#1.dummy_ earthquake_1]+_b[1.dummyF_1#1.female#1.dummy_earthquake_1]+ _b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1] +_b[1.dummyF_1#1.female#1.testseverity#1.dummy_earthquake_1]


permute knee perm8knee=_b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1#1.female], saving(perm8knee.dta, replace) reps(1000) left nodrop:xtreg knee yeduca i.female##i.dummyF_1##i.testseverity##i.dummy_earthquake_1, vce(robust) fe


/*tests whether the females exposed have effects on knee size that are different from the females NOT exposed*/

test  _b[1.dummyF_1] +_b[ 1.dummyF_1#1.female] + _b[1.dummyF_1#1.testseverity] + _b[1.dummyF_1#1.female#1.testseverity] + _b[1.dummyF_1#1.dummy_ earthquake_1]+_b[1.dummyF_1#1.female#1.dummy_earthquake_1]+ _b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1] +_b[1.dummyF_1#1.female#1.testseverity#1.dummy_earthquake_1]- (_b[1.female#1.testseverity]+_b[1.female#1.dummy_earthquake]+_b[1.female#1.dummy_earthquake_1#1.testseverity])

local diffknee= _b[1.dummyF_1] +_b[ 1.dummyF_1#1.female] + _b[1.dummyF_1#1.testseverity] + _b[1.dummyF_1#1.female#1.testseverity] + _b[1.dummyF_1#1.dummy_ earthquake_1]+_b[1.dummyF_1#1.female#1.dummy_earthquake_1]+ _b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1] +_b[1.dummyF_1#1.female#1.testseverity#1.dummy_earthquake_1]- (_b[1.female#1.testseverity]+_b[1.female#1.dummy_earthquake]+_b[1.female#1.dummy_earthquake_1#1.testseverity])



/*HEIGHT*/

xtreg adjheight yeduca i.dummyF_1##i.female##i.testseverity##dummy_earthquake_1,vce(robust) fe

predict pheight_fluearth_testsever_all

test  _b[1.dummyF_1] +_b[ 1.dummyF_1#1.female] + _b[1.dummyF_1#1.testseverity] + _b[1.dummyF_1#1.female#1.testseverity] + _b[1.dummyF_1#1.dummy_ earthquake_1]+_b[1.dummyF_1#1.female#1.dummy_earthquake_1]+ _b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1] +_b[1.dummyF_1#1.female#1.testseverity#1.dummy_earthquake_1]=0

local beta3height= _b[1.dummyF_1] +_b[ 1.dummyF_1#1.female] + _b[1.dummyF_1#1.testseverity] + _b[1.dummyF_1#1.female#1.testseverity] + _b[1.dummyF_1#1.dummy_ earthquake_1]+_b[1.dummyF_1#1.female#1.dummy_earthquake_1]+ _b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1] +_b[1.dummyF_1#1.female#1.testseverity#1.dummy_earthquake_1]

permute adjheight perm8height=_b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1#1.female], saving(perm8height.dta, replace) reps(1000) left nodrop:xtreg adjheight yeduca i.female##i.dummyF_1##i.testseverity##i.dummy_earthquake_1, vce(robust) fe

/*tests whether the females exposed have effects on height that are different from the females NOT exposed*/

test  _b[1.dummyF_1] +_b[ 1.dummyF_1#1.female] + _b[1.dummyF_1#1.testseverity] + _b[1.dummyF_1#1.female#1.testseverity] + _b[1.dummyF_1#1.dummy_ earthquake_1]+_b[1.dummyF_1#1.female#1.dummy_earthquake_1]+ _b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1] +_b[1.dummyF_1#1.female#1.testseverity#1.dummy_earthquake_1]- (_b[1.female#1.testseverity]+_b[1.female#1.dummy_earthquake]+_b[1.female#1.dummy_earthquake_1#1.testseverity])

local diffheight= _b[1.dummyF_1] +_b[ 1.dummyF_1#1.female] + _b[1.dummyF_1#1.testseverity] + _b[1.dummyF_1#1.female#1.testseverity] + _b[1.dummyF_1#1.dummy_ earthquake_1]+_b[1.dummyF_1#1.female#1.dummy_earthquake_1]+ _b[1.dummyF_1#1.testseverity#1.dummy_earthquake_1] +_b[1.dummyF_1#1.female#1.testseverity#1.dummy_earthquake_1]- (_b[1.female#1.testseverity]+_b[1.female#1.dummy_earthquake]+_b[1.female#1.dummy_earthquake_1#1.testseverity])



/************************************************************************/
/****************F-Wald tests for sums of coefficientys******************/
/************************************************************************/

/*we use the command "test" followed by expression we want to test:

test _b[namevar1] +_b[namevar2]=b

For example 

test _b[1.dummyF_1]+ _b[1.dummyF_1#3.severity]=0

See file FINAL_RESULTS_AUGUST_2020.docx


/************************************************************************/
/*************************FOR GRAPHS*************************************/
/************************************************************************/

/*****CREATES CATEGORIES OF EXPOSURE FOR ALL****/

/*

the following only used for models for females: we use the variable SEVERITY
these are categories to plot predicted values pknee_flu_sever_females 
and pheight_sever_females

*/

gen exposed_flu_sever_female=.
replace exposed_flu_sever_female=1 if dummyF_1==1&severity==1
replace exposed_flu_sever_female=2 if dummyF_1==1&severity==2
replace exposed_flu_sever_female=3 if dummyF_1==1&severity==3
replace exposed_flu_sever_female=4 if dummyF_1==0&severity==1
replace exposed_flu_sever_female=5 if dummyF_1==0&severity==2
replace exposed_flu_sever_female=6 if dummyF_1==0&severity==3
/*

The following only used for models with entire sample and when using NEWSEVERITY
these are categories to plot predicted values  pknee_flu_newsever_all  and pheight_newsever_allfemales

*/


gen exposed_flu_newsever_all=.
replace exposed_flu_newsever_all=1 if dummyF_1==1&newseverity==0
replace exposed_flu_newsever_all=2 if dummyF_1==1&newseverity==1
replace exposed_flu_newsever_all=3 if dummyF_1==0&newseverity==0
replace exposed_flu_newsever_all=4 if dummyF_1==0&newseverity==1

/*

The following only used for models with entire sample including TESTSEVERITY and EARTHQUAKE

These are categories to plot predicted values  pknee_fluearth_testsever_all  and pheight_flu_testsever_all

*/

gen exposed_fluearth_testsever_all=.

replace exposed_fluearth_testsever_all=1 if dummyF_1==1&dummy_earthquake_1==1&testseverity==0
replace exposed_fluearth_testsever_all=2 if dummyF_1==1&dummy_earthquake_1==1&testseverity==1
replace exposed_fluearth_testsever_all=3 if dummyF_1==1&dummy_earthquake_1==0&testseverity==0
replace exposed_fluearth_testsever_all=4 if dummyF_1==1&dummy_earthquake_1==0&testseverity==1
replace exposed_fluearth_testsever_all=5 if dummyF_1==0&dummy_earthquake_1==1&testseverity==0
replace exposed_fluearth_testsever_all=6 if dummyF_1==0&dummy_earthquake_1==1&testseverity==1
replace exposed_fluearth_testsever_all=7 if dummyF_1==0&dummy_earthquake_1==0&testseverity==0
replace exposed_fluearth_testsever_all=8 if dummyF_1==0&dummy_earthquake_1==0&testseverity==1


/*


graph box pknee_flu_newsever_all, over(exposed_flu_newsever_all)


*/
log close

end
 
