
# Impacts of the 1918 flu on survivors' nutritional status: a double quasi-natural experiment

By A. Palloni, M.McEniry, Yiyue Huangfu and Hiram Beltran-Sanchez.

## Code to estimate main models and produce graphs
- `code/INAL-PRFLU-MODELS-AUGUST-2020.do`

## Data file use for estimations
- `data/FINAL-PRFLU-DATA-AUGUST-2020.DTA`

## Document containing output of estimation
- `output/FINAL-RESULTS-AUGUST-2020.DOCX`